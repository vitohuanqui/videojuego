#ifndef VIDEOJUEGO_METEORITES_H
#define VIDEOJUEGO_METEORITES_H

#include <math.h>
#include <vector>

class Meteorites {
public:
    float pos_x;
    float pos_y;
    float pos_z;
    float angle;

    Meteorites(float _angle, float x, float y, float z) {
        angle = _angle;
        pos_x = x;
        pos_y = y;
        pos_z = z;
    }

    void draw() {
        glColor3d(255, 175, 0);
        glPushMatrix();
        glTranslatef(pos_x, pos_y, pos_z);
        glRotated(angle, 1, 1 , 0);
        glutSolidSphere(1.5, 8, 8);
        glPopMatrix();
    }

    bool move(float dt, SpacecraftPlayer* player, bool & intercalate) {
        unsigned long long int clockCount;
        __asm__ volatile (".byte 0x0f, 0x31" : "=A" (clockCount));
        srand((unsigned) clockCount);
        if (-26 < this->pos_x and this->pos_x < 26 and -24 < this->pos_y and this->pos_y < 26) {
            this->pos_x += cos(angle) * dt;
            this->pos_y += sin(angle) * dt;
        } else {
            this->angle = (rand() % 180) + 180;
            if (180 <= this->angle and this->angle <= 270) {
                if (intercalate) {
                    if (rand()%2)
                        this->pos_x = (-1)*25;
                    else
                        this->pos_x = 25;
                    this->pos_y = (rand() % 15) + 10;
                    intercalate = 0;
                   // printf("A cambio angle = %f x: %f y: %f z: %f \n", this->angle, this->pos_x, this->pos_y, this->pos_z);
                } else {
                    this->pos_x = (rand() % 50) - 25;
                    this->pos_y = 25;
                    intercalate = 1;
                    //printf("B cambio angle = %f x: %f y: %f z: %f \n", this->angle, this->pos_x, this->pos_y, this->pos_z);
                }
            }
        }
        this->draw();
        return this->colition(player);
    }
    bool colition(SpacecraftPlayer* player){
        float dist = (player->pos_x-this->pos_x)*(player->pos_x-this->pos_x) +
                     (player->pos_y-this->pos_y)*(player->pos_y-this->pos_y)/* +
                     (player.pos_z-this->pos_z)*(player.pos_z-this->pos_z)*/;
        if ( sqrt(dist) >= (player->size + 1.5) ){
            return 0;
        }
        return 1;
    }
};

#endif //VIDEOJUEGO_METEORITES_H
