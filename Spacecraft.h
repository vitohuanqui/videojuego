#ifndef VIDEOJUEGO_SPACECRAFT_H
#define VIDEOJUEGO_SPACECRAFT_H

#include <iostream>
#include <vector>

using namespace std;

class Missile {
public:
    float pos_x;
    float pos_y;
    float pos_z;
    int direction;
    int lost = 0;

    Missile(float x, float y, float z, int _direction){
        pos_x = x;
        pos_y = y;
        pos_z = z;
        direction = _direction;
    }
    void move(float dt){
        if (26 > this->pos_y and -24 < this->pos_y)
            this->pos_y += direction*dt;
    }
    void guided_move(float dt, int x, int y){
        if (lost){
            this->move(dt);
        }
        else{
            if (this->pos_x < x){
                this->pos_x += dt;
            }
            else if (this->pos_x > x){
                this->pos_x -= dt;
            }
            if (this->pos_y < y){
                lost = 1;
            }
            else if (this->pos_y > y) {
                this->pos_y -= dt;
            }
        }
    }
    void draw(){
        glColor3d(255, 255, 0);
        glPushMatrix();
        glTranslatef(pos_x, pos_y, pos_z);
        glutSolidCube(1);
        glPopMatrix();
    }
};

class Spacecraft {
public:
    float pos_x;
    float pos_y;
    float pos_z;
    int size;


    Spacecraft(float x, float y, float z, int _size) {
        pos_x = x;
        pos_y = y;
        pos_z = z;
        size = _size;
    }
    Spacecraft() {
        pos_x = 0;
        pos_y = 0;
        pos_z = 0;
        size = 2;
    }
    void move_x_negative(float dt) {
        if (-23 < this->pos_x) {
            this->pos_x -= dt;
        }
    }
    void move_x_positive(float dt) {
        if (23 > this->pos_x) {
            this->pos_x += dt;
        }
    }
    void move_y_negative(float dt){
        if (-22 < this->pos_y)
            this->pos_y -= dt;
    }
    void move_y_positive(float dt){
        if (8 > this->pos_y)
            this->pos_y += dt;
    }

    void draw() {
        glColor3d(255, 0, 0);
        glPushMatrix();
        glTranslatef(pos_x, pos_y, pos_z);
        glutSolidCube(size);
        glPopMatrix();
    }
};


class SpacecraftPlayer : public Spacecraft{
public:

    int score;
    int lives;
    int bombs;
    int upgrade;
    int num_shots;

    SpacecraftPlayer (float x, float y, float z, int _size) {
        pos_x = x;
        pos_y = y;
        pos_z = z;
        size = _size;
        score = bombs = upgrade = 0;
        lives = num_shots = 3;
    }
    vector<Missile> missiles;
    void shot(){
        Missile missile_tmp(this->pos_x,this->pos_y,this->pos_z, 1);
        missile_tmp.draw();
        missiles.push_back(missile_tmp);
    }
    void draw_missiles(float dt){
        for (int i=0; i<missiles.size();i++){
            if (24 < missiles[i].pos_y){
                missiles.erase(missiles.begin()+i);
            }
            missiles[i].move(dt);
            missiles[i].draw();
        }
    }
    void player_destroy(){
        printf("NxOO %d \n ", lives);
        lives--;
        if (!lives){
            printf("NPOOO %d \n ", lives);
            end_game = 1;
        }
    }
};

class SpacecraftEnemy : public Spacecraft{
public:
    int level;
    float goto_x;
    float goto_y;
    int ratio = 0;
    float angle = 0;

    SpacecraftEnemy (float x, float y, float z, int _size, int lvl) {

        unsigned long long int clockCount;
        __asm__ volatile (".byte 0x0f, 0x31" : "=A" (clockCount));
        srand((unsigned) clockCount);
        this->goto_x = (rand() % 45) - 20;
        this->goto_y = (rand() % 13) + 8;

        pos_x = x;
        pos_y = y;
        pos_z = z;
        size = _size;
        level = lvl;
    }
    vector<Missile> missiles;
    void shot(){
        Missile missile_tmp(this->pos_x,this->pos_y,this->pos_z, -1);
        missile_tmp.draw();
        missiles.push_back(missile_tmp);
    }
    void destroy_enemy(){
        printf("destroy %d \n", this->level);
    }
    void draw_missiles(float dt, SpacecraftPlayer player){
        for (int i=0; i<missiles.size();i++){
            if (-24 > missiles[i].pos_y){
                missiles.erase(missiles.begin()+i);
            }
            if (level == 1)
                missiles[i].move(dt);
            else
                missiles[i].guided_move(dt, player.pos_x, player.pos_y);
            missiles[i].draw();
        }
    }
    void move_1(float dt){
        if (this->pos_x < this->goto_x){
            this->pos_x += dt;
        }
        else if (this->pos_x > this->goto_x){
            this->pos_x -= dt;
        }
        else{
            unsigned long long int clockCount;
            __asm__ volatile (".byte 0x0f, 0x31" : "=A" (clockCount));
            srand((unsigned) clockCount);
            this->goto_x = (rand() % 45) - 20;
        }
        if (this->pos_y < this->goto_y){
            this->pos_y += dt;
        }
        else if (this->pos_y > this->goto_y){
            this->pos_y -= dt;
        }
        else{
            unsigned long long int clockCount;
            __asm__ volatile (".byte 0x0f, 0x31" : "=A" (clockCount));
            srand((unsigned) clockCount);
            this->goto_y = (rand() % 13) + 8;
        }
    }
    void move_3(float dt, int x, int y){
        if (ratio){
            angle += (dt);
            this->pos_x = x+ratio*cos(angle);
            this->pos_y = y+ratio*sin(angle);
        }
        else{
            if (this->pos_x < x){
                this->pos_x += dt;
            }
            else if (this->pos_x > x){
                this->pos_x -= dt;
            }
            if (this->pos_y < y){
                this->pos_y += dt;
            }
            else if (this->pos_y > y) {
                this->pos_y -= dt;
            }
            float dist = (x-this->pos_x)*(x-this->pos_x) +
                         (y-this->pos_y)*(y-this->pos_y)/* +
                     (player.pos_z-this->pos_z)*(player.pos_z-this->pos_z)*/;
            if ( sqrt(dist) <= 15 ){
                ratio = 15;
            }
        }
    }
    void move(float dt, SpacecraftPlayer player){
        if (level == 3){
            this->move_3(dt, player.pos_x, player.pos_y);
        }
        else{
            this->move_1(dt);
        }
    }
};

#endif //VIDEOJUEGO_SPACECRAFT_H
