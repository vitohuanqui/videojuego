
#ifndef VIDEOJUEGO_UTILS_H
#define VIDEOJUEGO_UTILS_H

#include "Spacecraft.h"
#include "Meteorites.h"

bool colition(vector<Missile>& missiles, SpacecraftEnemy* enemy){
    float dist = 0;
    for (int i=0; i<missiles.size(); i++){
        dist = (enemy->pos_x-missiles[i].pos_x)*(enemy->pos_x-missiles[i].pos_x) +
               (enemy->pos_y-missiles[i].pos_y)*(enemy->pos_y-missiles[i].pos_y)/* +
                     (player.pos_z-this->pos_z)*(player.pos_z-this->pos_z)*/;
        if ( sqrt(dist) <= (enemy->size + 1) ){
            missiles.erase(missiles.begin()+i);
            return 1;
        }
        dist = 0;
    }
    return 0;
}

bool colition(vector<Missile>& missiles, SpacecraftPlayer* player){
    float dist = 0;
    for (int i=0; i<missiles.size(); i++){
        dist = (player->pos_x-missiles[i].pos_x)*(player->pos_x-missiles[i].pos_x) +
               (player->pos_y-missiles[i].pos_y)*(player->pos_y-missiles[i].pos_y)/* +
                     (player.pos_z-this->pos_z)*(player.pos_z-this->pos_z)*/;
        if ( sqrt(dist) <= (player->size + 1) ){
            missiles.erase(missiles.begin()+i);
            return 1;
        }
        dist = 0;
    }
    return 0;
}

bool colition(vector<Meteorites> meteorites, SpacecraftPlayer* player){
    float dist = 0;
    for (int i=0; i<meteorites.size(); i++){
        dist = (player->pos_x-meteorites[i].pos_x)*(player->pos_x-meteorites[i].pos_x) +
               (player->pos_y-meteorites[i].pos_y)*(player->pos_y-meteorites[i].pos_y)/* +
                     (player.pos_z-this->pos_z)*(player.pos_z-this->pos_z)*/;
        if ( sqrt(dist) <= (player->size + 1) ){
            return 1;
        }
        dist = 0;
    }
    return 0;
}

bool colition(SpacecraftPlayer* player, SpacecraftEnemy* enemy){
    float dist = 0;
    dist = (player->pos_x-enemy->pos_x)*(player->pos_x-enemy->pos_x) +
           (player->pos_y-enemy->pos_y)*(player->pos_y-enemy->pos_y)/* +
           (player.pos_z-this->pos_z)*(player.pos_z-this->pos_z)*/;
    if ( sqrt(dist) <= (player->size + enemy->size) ){
        return 1;
    }
    return 0;
}


#endif //VIDEOJUEGO_UTILS_H
