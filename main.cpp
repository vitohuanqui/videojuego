bool end_game = 0;
#define GLUT_DISABLE_ATEXIT_HACK
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <GL/glut.h>
#include <vector>

#include <math.h>

#include "Spacecraft.h"
#include "Meteorites.h"
#include "utils.h"

using namespace std;
#define RED 0
#define GREEN 0
#define BLUE 0

#define ALPHA 1

#define ECHAP 27
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);

GLvoid window_key(unsigned char key, int x, int y);
GLvoid callback_special(int key, int x, int y);
GLvoid callback_keyboard(unsigned char key, int x, int y);
GLvoid callback_mouse(int button, int state, int x, int y);


GLvoid callback_motion(int x, int y);

//function called on each frame
GLvoid window_idle();
int time_ = 0;
int timebase = 0;
float dt = 0;
float dt_acum = 0;
float dt_shot = 0;
int METEORITES = 10;
SpacecraftPlayer player(-20, 0, 0, 2);
vector<Meteorites> meteorites;
bool intercalate = 1;
SpacecraftEnemy enemy(0,10,0,2,1);

void create_meteorites(){
    srand(time(NULL));
    for (int i=0;i<METEORITES; i++){
        Meteorites tmp(rand()%180+180, rand()%46-25, rand()%16+10, 0);
        //Meteorites tmp(rand()%360, -20, 0 , 0);
        meteorites.push_back(tmp);
    }
}

void destroy_all(){
    end_game = 1;
    printf("FINFIN \n");
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);


    glutInitWindowSize(800, 1200);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("VideoJuego");


    initGL();
    init_scene();

    //GL_RGBA : en caso de una textura con transparencia sino GL_RGB
    //GL_BGRA_EXT o GL_BGR_EXT : utilizar en caso de inversion de colores sino GL_RGBA o GL_RGB

    glutSpecialFunc(&callback_special);
    glutKeyboardFunc(&callback_keyboard);
    glutMouseFunc(&callback_mouse);
    glutMotionFunc(&callback_motion);

    create_meteorites();

    glutDisplayFunc(&window_display);

    glutReshapeFunc(&window_reshape);

    //glutKeyboardFunc(&window_key);

    //function called on each frame
    glutIdleFunc(&window_idle);

    glutMainLoop();

    return 1;
}



GLvoid initGL()
{
    GLfloat position[] = { 0.0f, 5.0f, 10.0f, 0.0 };

    //enable light : try without it
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glEnable(GL_LIGHTING);
    //light 0 "on": try without it
    glEnable(GL_LIGHT0);

    //shading model : try GL_FLAT
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);

    //enable material : try without it
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glClearColor(RED, GREEN, BLUE, ALPHA);
}

void enviroment()
{
    if (end_game){
    }
    else{
        float vel_missiles = dt;
        enemy.draw();
        player.draw();
        player.draw_missiles(dt);
        if (enemy.level > 1)
            vel_missiles = dt/10;
        enemy.draw_missiles(vel_missiles, player);
        for (int i=0; i<METEORITES; i++){
            if (meteorites[i].move(dt/10, &player, intercalate))
                destroy_all();
        }
        if (colition(enemy.missiles, &player)){
            player.player_destroy();
        }
        if (colition(player.missiles, &enemy)){
            player.score++;
            enemy.level++;
            player.bombs++;
            enemy.destroy_enemy();
        }
        if (colition(&player, &enemy)){
            end_game = 1;
        }
        if (dt_acum > 20){
            enemy.move(dt, player);
            dt_acum = 0;
        }
        if (dt_shot > 30){
            enemy.shot();
            dt_shot = 0;
        }
    }
}

GLvoid callback_special(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_UP:			// et on demande le réaffichage.
            player.move_y_positive(dt);
            break;

        case GLUT_KEY_DOWN:			// et on demande le réaffichage.
            player.move_y_negative(dt);
            break;

        case GLUT_KEY_LEFT:			// et on demande le réaffichage.
            player.move_x_negative(dt);
            break;

        case GLUT_KEY_RIGHT:			// et on demande le réaffichage.
            player.move_x_positive(dt);
            break;
        case 114:
            printf("BOMBO \n");
            break;
    }
}

GLvoid callback_keyboard(unsigned char key, int x, int y)
{
    if (key == 32) {
        player.shot();
        printf("SHOT\n");
    }
}


///////////////////////////////////////////////////////////////////////////////
//(2)
///////////////////////////////////////////////////////////////////////////////
GLvoid callback_mouse(int button, int state, int x, int y)
{
}

///////////////////////////////////////////////////////////////////////////////
//(3)
///////////////////////////////////////////////////////////////////////////////
GLvoid callback_motion(int x, int y)
{
}





GLvoid window_display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //gluPerspective(80, 1, 0.1, 40);
    //glTranslatef(0, 0, -30);
    glOrtho(-25.0f,  25.0f,-25.0f, 25.0f, -1.0f, 1.0f);


    /*dibujar aqui*/

    //exercise1();


    time_ = glutGet(GLUT_ELAPSED_TIME);
    dt = (time_ -timebase)/50.0f;
    timebase = time_;
    dt_acum += dt;
    dt_shot += dt;

    enviroment();

    glutSwapBuffers();

    glFlush();
}

GLvoid window_reshape(GLsizei width, GLsizei height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);

    glMatrixMode(GL_MODELVIEW);
}



void init_scene()
{

}


//function called on each frame
GLvoid window_idle()
{


    glutPostRedisplay();
}